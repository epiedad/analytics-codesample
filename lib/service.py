import argparse
from django.conf import settings

from apiclient.discovery import build
from oauth2client import client, file, tools
from httplib2 import Http

GOOGLE_WEBMASTER_TOOLS = settings.GOOGLE_WEBMASTER_TOOLS


class GoogleWebmasterService(object):
    """
    Class object for google webmaster service
    """

    def __init__(self, version=None, key_file=None, scopes=None, build_service=False):

        self.version = version or GOOGLE_WEBMASTER_TOOLS['VERSION']
        self.client_secrets_file = key_file or GOOGLE_WEBMASTER_TOOLS['CLIENT_SECRETS_FILE_PATH']

        self.scopes = scopes or GOOGLE_WEBMASTER_TOOLS['SCOPES']

        # initialize analytics service
        self.service = None
        if build_service:
            self.build_service()

    def build_service(self):
        """Initializes/Build a Webmaster service object.
        Returns:
             analytics an authorized analytics service object.
        """
        client_secrets = self.client_secrets_file
        # Set up a Flow object to be used if we need to authenticate.
        flow = client.flow_from_clientsecrets(client_secrets,
                                              scope=self.scopes,
                                              message=tools.message_if_missing(client_secrets))

        # Prepare credentials, and authorize HTTP object with them.
        # If the credentials don't exist or are invalid run through the native client
        # flow. The Storage object will ensure that if successful the good
        # credentials will get written back to a file.
        dat_file = GOOGLE_WEBMASTER_TOOLS['DAT_FILE_PATH']
        storage = file.Storage(dat_file)
        credentials = storage.get()

        # Parser command-line arguments.
        parent_parsers = [tools.argparser]
        parser = argparse.ArgumentParser(
            description='Google Webmaster Tools',
            formatter_class=argparse.RawDescriptionHelpFormatter,
            parents=parent_parsers)
        flags = parser.parse_args([])

        if credentials is None or credentials.invalid:
            credentials = tools.run_flow(flow, storage, flags=flags)
        http = credentials.authorize(http=Http())

        self.service = build('webmasters', self.version, http=http)

    def execute_request(self, property_uri, request):
        """Executes a searchAnalytics.query request.
        Args:
          property_uri: The site or app URI to request data for.
          request: The request to be executed.
        Returns:
          An array of response rows.
        """
        return self.service.searchanalytics().query(
            siteUrl=property_uri, body=request).execute()

    def get_site_list(self):
        """
        Get Webmaster tools Execute Site list
        :return:
        """
        site_list = self.service.sites().list().execute()
        return site_list
