from __future__ import absolute_import
from django.conf.urls import url

from .views import (GoogleWebMasterQueryViewAPI, GoogleWebMasterPageViewAPI, WebmasterMediaToolsPropertyView,
                    GoogleWebMasterDeviceViewAPI, GoogleWebMasterCountryViewAPI)

# google webmaster tools reports api
urlpatterns = [
    url(r"^property/(?P<slug>([-\w]+))/?$", WebmasterMediaToolsPropertyView.as_view(), name="property"),
    url(r"^query/$", GoogleWebMasterQueryViewAPI.as_view(), name="api_query"),
    url(r"^page/$", GoogleWebMasterPageViewAPI.as_view(), name="api_page"),
    url(r"^country/$", GoogleWebMasterCountryViewAPI.as_view(), name="api_country"),
    url(r"^device/$", GoogleWebMasterDeviceViewAPI.as_view(), name="api_device"),
]
