import json
import argparse
from httplib2 import Http
from django.conf import settings
from django.core.urlresolvers import reverse

from rest_framework.test import APITestCase
from rest_framework.status import HTTP_200_OK
from apiclient.discovery import build
from oauth2client import client, file, tools

from google_webmaster_tools.lib.service import GoogleWebmasterService


START_DATE = '2016-01-01'
END_DATE = '2016-12-30'
PROPERTY_URI = 'http://www.madchildcare.com.au'


class TestGoogleWebMasterTools(APITestCase):
    """
    Test Cases for Implementation for Google Webmaster tools
    """
    def test_settings_setup(self):
        """
        Check if local settings setup
        :return:
        """
        self.assertIn('google_analytics_reports', settings.INSTALLED_APPS)
        required_settings = ['SCOPES', 'CLIENT_SECRETS_FILE_PATH', 'VERSION', 'DAT_FILE_PATH']
        for req in required_settings:
            self.assertIn(req, settings.GOOGLE_WEBMASTER_TOOLS)

    def test_google_webmaster_tools_service(self):
        """
        Test Google Webmaster tools service instance
        :return:
        """
        scope = 'https://www.googleapis.com/auth/webmasters.readonly'
        version = 'v3'
        # Name of a file containing the OAuth 2.0 information for this
        # application, including client_id and client_secret, which are found
        # on the API Access tab on the Google APIs
        # Console <http://code.google.com/apis/console>.
        client_secrets = settings.GOOGLE_WEBMASTER_TOOLS['CLIENT_SECRETS_FILE_PATH']

        # Set up a Flow object to be used if we need to authenticate.
        flow = client.flow_from_clientsecrets(client_secrets,
                                              scope=scope,
                                              message=tools.message_if_missing(client_secrets))

        # Prepare credentials, and authorize HTTP object with them.
        # If the credentials don't exist or are invalid run through the native client
        # flow. The Storage object will ensure that if successful the good
        # credentials will get written back to a file.
        dat_file = settings.GOOGLE_WEBMASTER_TOOLS['DAT_FILE_PATH']
        storage = file.Storage(dat_file)
        credentials = storage.get()

        # Parser command-line arguments.
        parent_parsers = [tools.argparser]
        parser = argparse.ArgumentParser(
            description='Webmaster Tools Test',
            formatter_class=argparse.RawDescriptionHelpFormatter,
            parents=parent_parsers)
        flags = parser.parse_args([])

        if credentials is None or credentials.invalid:
            credentials = tools.run_flow(flow, storage, flags=flags)
        http = credentials.authorize(http=Http())

        service = build('webmasters', version, http=http)

        self.assertTrue(service)
        site_list = service.sites().list().execute()
        self.assertTrue(site_list)
        self.assertIn('siteEntry', site_list)
        self.assertGreater(len(site_list['siteEntry']), 0)

    def test_webmaster_tools_service_class(self):
        """
        Testing Webmaster tools service class
        :return:
        """
        service_obj = GoogleWebmasterService()
        self.assertIsNone(service_obj.service)
        self.assertTrue(hasattr(service_obj, 'build_service'))
        service_obj.build_service()
        self.assertTrue(service_obj.service)
        self.assertTrue(hasattr(service_obj, 'get_site_list'))
        site_list = service_obj.get_site_list()
        self.assertTrue(site_list)
        self.assertIn('siteEntry', site_list)
        self.assertGreater(len(site_list['siteEntry']), 0)

    def test_webmasters_query_request(self):
        """
        Test Web master query request
        :return:
        """
        service_obj = GoogleWebmasterService(build_service=True)
        self.assertTrue(service_obj.service)

        # Get top 10 queries for the date range, sorted by click count, descending.
        request = {
            'startDate': START_DATE,
            'endDate': END_DATE,
            'dimensions': ['query'],
            'rowLimit': 10
        }
        self.assertTrue(hasattr(service_obj, 'execute_request'))
        response = service_obj.execute_request(PROPERTY_URI, request)
        self.run_test_response(response)

    def test_webmaster_tools_query_api_view(self):
        """
        Test Webmaster tools query api view
        Get top queries for the date range, sorted by click count, descending.
        :return:
        """
        post_data = {
            'propertyUri': PROPERTY_URI,
            'startDate': START_DATE,
            'endDate': END_DATE,
            'rowLimit': 10,
            'startRow': 0
        }
        api_url = reverse('ga_webmaster_tools:api_query')
        r = self.client.post(api_url, post_data, follow='json')
        self.assertEqual(r.status_code, HTTP_200_OK)
        self.assertNotContains(r, 'error')
        response = json.loads(r.content)
        self.run_test_response(response)

    def test_webmaster_tools_device_api_view(self):
        """
        Testing Google webmaster tools device api view
        :return:
        """
        post_data = {
            'propertyUri': PROPERTY_URI,
            'startDate': START_DATE,
            'endDate': END_DATE,
            'rowLimit': 10,
            'startRow': 0
        }
        api_url = reverse('ga_webmaster_tools:api_device')
        r = self.client.post(api_url, post_data, follow='json')
        self.assertEqual(r.status_code, HTTP_200_OK)
        self.assertNotContains(r, 'error')
        response = json.loads(r.content)
        self.run_test_response(response)

    def test_webmaster_tools_country_api_view(self):
        """
        Testing Google webmaster country api view
        :return:
        """
        post_data = {
            'propertyUri': PROPERTY_URI,
            'startDate': START_DATE,
            'endDate': END_DATE,
            'rowLimit': 10,
            'startRow': 0
        }
        api_url = reverse('ga_webmaster_tools:api_country')
        r = self.client.post(api_url, post_data, follow='json')
        self.assertEqual(r.status_code, HTTP_200_OK)
        self.assertNotContains(r, 'error')
        response = json.loads(r.content)
        self.run_test_response(response)

    def test_webmaster_tools_page_api_view(self):
        """
        Get top pages for the date range, sorted by click count, descending.
        :return:
        """
        post_data = {
            'propertyUri': PROPERTY_URI,
            'startDate': START_DATE,
            'endDate': END_DATE,
            'rowLimit': 10,
            'startRow': 0
        }
        api_url = reverse('ga_webmaster_tools:api_page')
        r = self.client.post(api_url, post_data, follow='json')
        self.assertEqual(r.status_code, HTTP_200_OK)
        self.assertNotContains(r, 'error')
        response = json.loads(r.content)
        self.run_test_response(response)

    def run_test_response(self, response):

        self.assertTrue(response)
        self.assertIn('rows', response)
        expected_query_response_keys = [u'keys', u'impressions', u'clicks', u'ctr', u'position']
        for r in response['rows']:
            self.assertEqual(expected_query_response_keys, r.keys())
