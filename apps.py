from __future__ import unicode_literals

from django.apps import AppConfig


class GoogleWebmasterToolsConfig(AppConfig):
    name = 'google_webmaster_tools'
