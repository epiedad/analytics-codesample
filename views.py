import datetime

from django.views.generic import TemplateView
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import redirect

from helpers.api_helper import LoginRequiredViewMixin
from rest_framework.response import Response

from helpers.api_helper import BaseGoogleAPIView
from google_webmaster_tools.lib.service import GoogleWebmasterService
from google_analytics_reports.models import MediaPropertyWebsites


class GoogleWebMasterToolsAPIMixin(object):
    """
    Google Web Master Tools API view mixin
    """
    dimensions = None

    def post(self, request, *args, **kwargs):
        property_uri = request.data.get('propertyUri', )
        start_date = request.data.get('startDate', '')
        now = datetime.datetime.now()
        end_date = request.data.get('endDate', now.strftime('%Y-%m-%d'))
        row_limit = request.data.get('rowLimit', 5000)  # Valid range 1-5000
        start_row = request.data.get('startRow', 0)
        ga_request_body = {
            'startDate': start_date,
            'endDate': end_date,
            'dimensions': self.dimensions,
            'rowLimit': row_limit,
            'startRow': start_row
        }
        # check if theres a extra params and add it in request body
        extra_params = request.data.get('extraParams', None)
        if extra_params:
            ga_request_body.update(extra_params)

        res = self.execute_request(property_uri, ga_request_body)
        return Response(res)

    def execute_request(self, property_uri, request_body):
        service_obj = GoogleWebmasterService(build_service=True)
        response = service_obj.execute_request(property_uri, request_body)
        return response


class GoogleWebMasterQueryViewAPI(GoogleWebMasterToolsAPIMixin, BaseGoogleAPIView):
    """
    Google Web Master Query View API
    Get top queries for the date range, sorted by click count, descending.
    """
    dimensions = ['query']


class GoogleWebMasterPageViewAPI(GoogleWebMasterToolsAPIMixin, BaseGoogleAPIView):
    """
    Google Web Master Page View API
    Get top pages for the date range, sorted by click count, descending.
    """
    dimensions = ['page']


class GoogleWebMasterCountryViewAPI(GoogleWebMasterToolsAPIMixin, BaseGoogleAPIView):
    """
    Google Web Master Tools Countries View API
    """
    dimensions = ['country']


class GoogleWebMasterDeviceViewAPI(GoogleWebMasterToolsAPIMixin, BaseGoogleAPIView):
    """
    Google Web Master Tools Device View API
    """
    dimensions = ['device']


class WebmasterMediaToolsPropertyView(LoginRequiredViewMixin, TemplateView):
    """
    Template View for Web Master tools media property view
    """
    template_name = 'webmaster_tools/property.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        if 'property' not in context:
            return redirect('dashboard')

        return self.render_to_response(context)

    def get_context_data(self, **kwargs):

        slug = kwargs.get('slug', '')
        context = super(WebmasterMediaToolsPropertyView, self).get_context_data(**kwargs)

        try:
            website = MediaPropertyWebsites.objects.get(slug_url=slug)
        except ObjectDoesNotExist:
            return context

        selected_property = {
                "id": '{}'.format(website.view_id),
                "name": '{}'.format(website.name),
                "website_url": '{}'.format(website.website_url),
                "slug_url": '{}'.format(website.slug_url)
            }
        context.update({
            "property": selected_property
        })
        return context
